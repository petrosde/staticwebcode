package com.amdocs.web.controller;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.amdocs.Calculator;
import com.amdocs.Increment;
import com.amdocs.config.SpringWebConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringWebConfig.class})
@WebAppConfiguration
public class HelloControllerTest {
 
    private MockMvc mockMvc;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
 
    @Test
    public void runHello() throws Exception {

        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                ;
        assertEquals("Result", 9, 9);
    }
    
    @Test
    public void runHelloName() throws Exception {

        mockMvc.perform(get("/hello/Prashant+Beniwal"))
                .andExpect(status().isOk())
                ;
        assertEquals("Result", 9, 9);
    }

    @Test
    public void runNegativeScen() throws Exception {

        mockMvc.perform(get("/try/Prashant+Beniwal"))
                .andExpect(status().isNotFound())
                ;
    }


    @Test
    public void calculatorConstructorTest() throws Exception {
        Calculator instance = new Calculator();
        int addition = new Calculator().add();
        assertEquals("addition", 9, addition);

        int substitution = new Calculator().sub();
        assertEquals(substitution, 3, substitution);
    }


    @Test
    public void decreasecounterTest() throws Exception {
        int res = new Increment().decreasecounter(0);
        assertEquals("res1", 1, res);

        int res2 = new Increment().decreasecounter(1);
        assertEquals("res2", 1, res2);

        int res3 = new Increment().decreasecounter(10);
        assertEquals("res3", 1, res3);
    }
}
